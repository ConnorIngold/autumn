import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import bulma from "bulma";

import "./registerServiceWorker";

Vue.config.productionTip = false;

new Vue({
  router,
  bulma,
  render: h => h(App)
}).$mount("#app");
